from preprocessingLib import *

from xml.dom import minidom
from xml.etree.ElementTree import Element, SubElement, Comment, tostring, tostringlist, ElementTree

#from neuralcoref import Coref
from preprocessingLib import _lemmatize

example1 = "Idiopathic intracranial hypertension (IIH) is a condition characterized by increased intracranial pressure (pressure around the brain) without a detectable cause."

example2 = '''Idiopathic intracranial hypertension (IIH) is a condition characterized by increased intracranial pressure (pressure around the brain) without a detectable cause.
Risk factors include being overweight or a recent increase in weight. Tetracycline may also trigger the condition. The diagnosis is based on symptoms and a high intracranial pressure founding during a lumbar puncture with no specific cause found on a brain scan.'''

example3 = '''Idiopathic intracranial hypertension (IIH) is a condition characterized by increased intracranial pressure (pressure around the brain) without a detectable cause. The main symptoms are headache, vision problems, ringing in the ears with the heartbeat, and shoulder pain. Complications may include vision loss.
Risk factors include being overweight or a recent increase in weight. Tetracycline may also trigger the condition. The diagnosis is based on symptoms and a high intracranial pressure founding during a lumbar puncture with no specific cause found on a brain scan.
Treatment includes a healthy diet, salt restriction, and exercise. Bariatric surgery may also be used to help with weight loss. The medication acetazolamide may also be used along with the above measures. A small percentage of people may require surgery to relieve the pressure.
About 2 per 100,000 people are newly affected per year. The condition most commonly affects women aged 20–50. Women are affected about 20 times more often than men. The condition was first described in 1897.'''

paragraphs_counter = 1
sentences_counter = 1
words_counter = 1
chunks_counter = 1


def indent(elem, level=0):
    i = "\n" + level * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def prettify(elem):
    '''
        This function will receive as input the root of the XML
        and give as output the pretty printable version of that XML.
    '''

    indent(elem)

    tree = ElementTree(elem)

    tree.write("lol.xml", xml_declaration=True, encoding='utf-8', method="xml")


 #   rough_string = tostring(elem, encoding="UTF-8", method="xml")
  #  reparsed = minidom.parseString(rough_string)
  #  return reparsed.toprettyxml(indent="  ")


def create_chunk_nodes(sent, chunked_words, pos_tags):
    """
            This function will receive as input the parent paragraph
            and the sentences of that paragraph.
            It will create the chunk nodes
            and put the corresponding content into them.
    """

    global chunks_counter

    c = SubElement(sent, 'CHUNK', {'ID': 'CHUNK' + str(chunks_counter)})

    create_word_nodes(c, chunked_words, pos_tags)

    chunks_counter += 1


def create_word_nodes(tag, words, pos_tags):
    '''
        This function will receive as input the parent sentence
        and the words of that sentence + pos tags ...(TODO)
        It will create the word nodes
        and put the corresponding content into them.
    '''

    global words_counter

    for pair in pos_tags:

        if pair[0] in punctMarks(words):
            w = SubElement(tag, 'W', {'ID': 'W' + str(words_counter),
                                          'POS': '.',
                                          'LEMMA': _lemmatize(pair[0], pair[1])
                                          })
        elif pair[0] in stopWordsList(words):
            w = SubElement(tag, 'W', {'ID': 'W' + str(words_counter),
                                          'POS': 'SW',
                                          'LEMMA': _lemmatize(pair[0], pair[1])
                                          })
        else:

            w = SubElement(tag, 'W', {'ID': 'W' + str(words_counter),
                                      'POS': pair[1],
                                      'LEMMA': _lemmatize(pair[0], pair[1])
                                       })
        w.text = pair[0]
        words_counter += 1



def clean_chunk_list(chunks):
    """
    This function gets as a parameter a chunks list. The list contains all the
    words, either tagged as chunks or not. The function will return only the
    chunked words
    """
    aux_chunk = []
    for chunk in chunks:
        if str(chunk).find("Chunk") > 0:
            aux_chunk.append(chunk)

    return aux_chunk

def create_chunk_word_nodes(sent, chunks):
    """
    A chunk is in essence a word, or a succession of words. This function
    determines if the next node in the XML should be a chunk(which will
    contain word notes of its own), or an ordinary word.
    """
    for chunk in chunks:
        if str(chunk).find("Chunk") > 0:
            chunked_words = []
            for pair in chunk:
                chunked_words.append(pair[0])
            create_chunk_nodes(sent,chunked_words,chunk)

        else:
            create_one_word_node(sent,chunk)

def create_one_word_node(sent, tuple):
    global words_counter
    if tuple[0] in punctMarks(tuple[0]):
        w = SubElement(sent, 'W', {'ID': 'W' + str(words_counter),
                                  'POS': '.',
                                  'LEMMA': _lemmatize(tuple[0], tuple[1])
                                  })
    elif tuple[0] in stopWordsList(tuple[0]):
        w = SubElement(sent, 'W', {'ID': 'W' + str(words_counter),
                                  'POS': 'SW',
                                  'LEMMA': _lemmatize(tuple[0], tuple[1])
                                  })
    else:

        w = SubElement(sent, 'W', {'ID': 'W' + str(words_counter),
                                  'POS': tuple[1],
                                  'LEMMA': _lemmatize(tuple[0], tuple[1])
                                  })
    w.text = tuple[0]
    words_counter += 1

"""
def create_chunk_or_word_nodes(sent, words, chunks, pos_tags):


    counter = 0
    chunk_freq = []

    chunks = clean_chunk_list(chunks)

    for j in range(len(chunks)):
        chunk_freq.append(0)

    i = 0
    while i < len(words):

        if counter >= len(chunks) or i >= len(words):
            break

        string_words = str(words[i])
        string_chunks = str(chunks[counter])

        if str(string_chunks).find(string_words, 0, len(string_chunks) ) > 0:

            if i + 1 < len(words):
                aux_index = i + 1
                chunked_words = []
                chunked_words.append(words[i])

                while str(chunks[counter]).find(str(words[aux_index])) > 0 and aux_index < len(words) and counter < len(chunks):
                    chunked_words.append(words[aux_index])
                    aux_index += 1
                    if aux_index >= len(words) or counter >= len(chunks):
                        break

                if chunk_freq[counter] == 0:
                    i = aux_index
                    create_chunk_nodes(sent, chunked_words, pos_tags)
                    chunk_freq[counter] = 1
                    counter = counter + 1
            else:
                create_chunk_nodes(sent, words[i], pos_tags)
                chunk_freq[counter] = 1
                counter = counter + 1
                i = i + 1
        else:
            non_chunked_words = []
            non_chunked_words.append(words[i])
            i += 1
            if i < len(words) or counter < len(chunks):

                while str(chunks[counter]).find(str(words[i])) == -1 and i < len(words) and counter < len(chunks):
                    non_chunked_words.append(words[i])
                    i += 1
                    if i >= len(words):
                        break

                create_word_nodes(sent, non_chunked_words, pos_tags)

"""

def create_sent_nodes(para, sentences):
    '''
        This function will receive as input the parent paragraph
        and the sentences of that paragraph.
        It will create the sentence nodes 
        and put the corresponding content into them.
    '''

    global sentences_counter
    for sentence in sentences:
        sent = SubElement(para, 'SENT', {'ID': 'SENT' + str(sentences_counter)})

        words = tokenize_sentence(sentence)
        chunks = chunking([sentence])

        pos_tags = posTaggerWord(sentence)

        create_chunk_word_nodes(sent, chunks)
        sentences_counter += 1


def create_para_nodes(root, paragraphs):
    '''
        This function will receive as input the root of the XML
        and the paragraphs.
        It will create the paragraph nodes 
        and put the corresponding content into them.
    '''

    global paragraphs_counter
    for paragraph in paragraphs:
        para = SubElement(root, 'PARA', {'ID': 'PARA' + str(paragraphs_counter)})

        sentences = tokenize_paragraph(paragraph)

        create_sent_nodes(para, sentences)

        paragraphs_counter += 1



def format_XML(text):

    l=fReader(text)
    '''
        This function will receive as input raw text
        and give as output the proccessed text in an XML format.
    '''

    root = Element('PREPROCESSED')

    paragraphs = separate_by_paragraphs(l)

    create_para_nodes(root, paragraphs)

    return root



def coreference_resolution(context_phrase, secondary_phrase):
    '''
            This function will receive as input a main phrase and a secondary one
            and gives as output a list of pairs of elements from the second phrase and the references to them form the
            main phrase
    '''

    coref = Coref()

    coref.one_shot_coref(utterances=secondary_phrase.decode('unicode_escape'),
                         context=context_phrase.decode('unicode_escape'))

    words_in_secondary_phrase = secondary_phrase.split()

    dictionary = coref.get_most_representative()

    dictionary = {str(k): str(v) for k, v in dictionary.iteritems()}

    keys = [key for key in dictionary]

    list_of_pairs = []

    for word in words_in_secondary_phrase:
        if word in keys:
            list_of_pairs.append([word, dictionary.get(word)])

    return list_of_pairs


if __name__ == "__main__":
    prettify(format_XML("kek.txt"))
